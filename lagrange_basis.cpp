#include "lagrange_basis.h"

#include <cassert>

namespace polynom {

LagrangeBasis::LagrangeBasis(const Matrix& XY, const Matrix& x):
    AbstractBasis(x.m()),
    _x(x)
{
    assert(x.m() > 1 && x.n() == 1);
    assert(XY.m() >= _n && XY.n() == 2);
    #ifndef NDEBUG
    // There are no two equal x values
    check_input();
    #endif // NDEBUG

    _coeffs = (XY.m() == x.m()) ? coeffs_interpol(XY) : coeffs_approx(XY);
}

LagrangeBasis::LagrangeBasis(const Matrix& xy):
    AbstractBasis(xy.m()),
    _x(xy.column(1))
{
    assert(xy.m() > 1 && xy.n() == 2);
    #ifndef NDEBUG
    // There are no two equal x values
    check_input();
    #endif // NDEBUG

    // Coefficients of interpolation are {Y_i} by definition.
    _coeffs = xy.column(2);
}

double LagrangeBasis::f(size_t i, double x) const
{
    assert(i > 0 && i <= _x.m());
    assert(_x.m() == _n);

    double result = 1;
    for (size_t j = 1; j <= _x.m(); ++j) {
        if(i == j) { continue; }
        result *= (x - _x(j)) / (_x(i) - _x(j));
    }
    return result;
}

#ifndef NDEBUG
void LagrangeBasis::check_input() const
{
    size_t m = _x.m();
    for(size_t i = 1; i <= m; ++i) {
        for(size_t j = i + 1; j <= m; ++j) {
            assert(_x(i) != _x(j));
        }
    }
}
#endif // NDEBUG

} // namespace polynom
