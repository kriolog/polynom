#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <memory>

#include <QMainWindow>

namespace polynom {
    class MuParser1DFunc;
    class AbstractBasis;
}

namespace polynom {

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /// Plot window.
    /// @param f a theory function
    /// @param pts_discr discrete measurement points (x,y)
    /// @param sigma a standard deviation of the measurement
    /// points (@a sigma >= 0)
    /// @param basis a regression (approximation or interpolation)
    /// function representrd by its decomposition by a basis
    /// @param parent a window parent
    explicit MainWindow(
        const MuParser1DFunc& f,
        const QPair<QVector<double>, QVector<double> >& pts_discr,
        double sigma,
        std::shared_ptr<AbstractBasis> basis,
        QWidget* parent = 0);
};

} // namespace polynom

#endif // MAINWINDOW_H

