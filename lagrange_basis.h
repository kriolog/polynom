#ifndef LAGRANGE_BASIS_H
#define LAGRANGE_BASIS_H

#include "abstract_basis.h"

#include "matrix.h"

namespace polynom {

/**
 * Decomposition by a Lagrange polynomial basis.
 */
class LagrangeBasis: public AbstractBasis
{
public:
    /**
     * Approximation or interpolation constructor, depending on the sizes of
     * @p XY and @p x: if size of XY == size of x - interpolation, otherwise
     * approximation. Size of XY should not be less than size of x, both should
     * be positive, all the x valuse should be different.
     * @param XY a data set (X, Y).
     * @param x reference basis points.
     * Number of points @a XY should not be less than the number of reference
     * basis points @a x.
     */
    LagrangeBasis(const Matrix& XY, const Matrix& x);

    /**
     * Interpolation constructor.
     * @param xy reference basis points and its function values: {x, y}.
     */
    LagrangeBasis(const Matrix& xy);

private:
    /**
     * Value of the @p i 'th Lagrange basis function in the point @p x.
     */
    virtual double f(size_t i, double x) const;

    #ifndef NDEBUG
    /**
     * Checks input validity. All the reference points must be different.
     */
    void check_input() const;
    #endif // NDEBUG

    /**
     *  Reference basis points.
     */
    Matrix _x;
};

} // namespace polynom

#endif // LAGRANGE_BASIS_H
