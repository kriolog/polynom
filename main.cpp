#include <iostream>
#include <random>
#include <memory>

#include "muParser.h"

#include <QApplication>
#include "mainwindow.h"

#include "utils.h"
#include "matrix.h"
#include "polynomial_basis.h"
#include "lagrange_basis.h"
#include "hermite_basis.h"

using namespace std;
using namespace polynom;

int main(int argc, char **argv) {

    /******** CLI: input the data ********/

    // current input string
    string s;

    // Choise of method: interpolation | approximation
    enum {interpol, approx} method;
    while(1) {
        cout << "Select a metod to be used: 1 - interpolation, 2 - approximation, q - quit\n(default=1): ";
        getline(cin, s);

        if(s == "q") {
            exit(EXIT_SUCCESS);
        } else if(s.empty() || s == "1") {
            method = interpol;
            break;
        } else if (s == "2") {
            method = approx;
            break;
        } // else try again
        cout << "\x1b[31m" << "error: " << "\033[0m" << "invalid value" << endl << endl;
    }
    cout << endl;

    // Choise of decomposition type: polynomial | Lagrange | Hermite
    enum {polynomial, lagrange, hermite} basis_type;
    while(1) {
        cout << "Select a basis type: 1 - polynomial, 2 - Lagrange, 3 - Hermite, q - quit\n(default=1): ";
        getline(cin, s);

        if(s == "q") {
            exit(EXIT_SUCCESS);
        } else if(s.empty() || s == "1") {
            basis_type = polynomial;
            break;
        } else if (s == "2") {
            basis_type = lagrange;
            break;
        } else if (s == "3") {
            basis_type = hermite;
            break;
        } // else try again
        cout << "\x1b[31m" << "error: " << "\033[0m" << "invalid value" << endl << endl;
    }
    cout << endl;

    // Choise of function f(x)
    // change x => evaluate f
    double mu_x = 0;
    mu::Parser mu_f;
    mu_f.DefineVar("x", &mu_x);
    string fun_str;

    while(1) {
        cout << "input an expression f(x), q - quit\n: ";
        getline(cin, fun_str);
        mu_f.SetExpr(fun_str);

        if(fun_str == "q") {
            exit(EXIT_SUCCESS);
        } else {
            try {
                mu_f.Eval();
            }
            catch (mu::Parser::exception_type &e) {
                cout << "\x1b[31m" << "error: " << "\033[0m" << e.GetMsg() << endl << endl;
                continue;
            }
        }
        break;
    }
    cout << endl;
    // wrapper around muParser function
    MuParser1DFunc f(mu_x, mu_f);

    // Interpolation + approximation: choise of segment [x_left, x_right]
    double x_l;
    double x_r;
    while(1) {
        cout << "Input domain boundaries (space separated):\n[x_left x_right], x_left < x_right, q - quit\n: ";
        getline(cin, s);

        if(s == "q") {
            exit(EXIT_SUCCESS);
        }
        stringstream ss(s);
        ss >> x_l >> x_r;
        char tmp;
        // !(two and only two doubles in the correct order).
        if(!(ss && (x_l < x_r) && !(ss >> tmp))) {
            cout << "\x1b[31m" << "error: " << "\033[0m" << "invalid values" << endl << endl;
            continue;
        }
        break;
    }
    cout << endl;

    // number of basis points (functions for polynomial)
    size_t n;
    // Interpolation + approximation: choise of number of basis points
    // (basis cardinality for polynomial): > 2
    while(1) {
        string msg;
        msg .append("Input a number of basis ")
            .append((basis_type == polynomial)
                ? "functions"
                : "points")
            .append(" n, n >= 2, q - quit\n(default=2): ");
        cout << msg;
        getline(cin, s);

        if(s == "q") {
            exit(EXIT_SUCCESS);
        } else if(s.empty()) {
            n = 2;
            break;
        }
        stringstream ss(s);
        long signed_n;
        ss >> signed_n;
        char tmp;
        // !(one and only one integer > 1).
        if(!(ss && (signed_n > 1) && !(ss >> tmp))) {
            cout << "\x1b[31m" << "error: " << "\033[0m" << "invalid value" << endl << endl;
            continue;
        }
        n = signed_n;
        break;
    }
    cout << endl;
    size_t basis_cardinality = ((basis_type != hermite) ? n : 2 * n);

    // Number of measurements (>= basis_cardinality)
    size_t m;
    // Approximation: choise of number of measurement points: >= number of basis
    // functions.
    if(method == approx) {
        while(1) {
            cout << "Input a number of measurement points m, "
                 << "m >= number of basis functios ("
                 << basis_cardinality << "), q - quit\n(default="
                 << basis_cardinality << "): ";
            getline(cin, s);

            if(s == "q") {
                exit(EXIT_SUCCESS);
            } else if(s.empty()) {
                m = basis_cardinality;
                break;
            }
            stringstream ss(s);
            long signed_m;
            ss >> signed_m;
            char tmp;
            // !(one and only one integer >= basis_cardinality).
            if(!(ss && (signed_m >= basis_cardinality) && !(ss >> tmp))) {
                cout << "\x1b[31m" << "error: " << "\033[0m" << "invalid value" << endl << endl;
                continue;
            }
            m = signed_m;
            break;
        }
        cout << endl;
    }

    double sigma;
    // Choise of Gaussian perturbation standard deviation (mean = 0). The values
    // of function will be randomly shifted with it.
    while(1) {
        cout << "Input a standard deviation sigma of Gaussian perturbation of\n"
             << "the function f() in the measurement points, q - quit"
            << "\n(default=0.1): ";
        getline(cin, s);

        if(s == "q") {
            exit(EXIT_SUCCESS);
        } else if(s.empty()) {
            sigma = 0.1;
            break;
        }
        stringstream ss(s);
        ss >> sigma;
        char tmp;
        // !(two and only two doubles in the correct order).
        if(!(ss && (sigma >= 0) && !(ss >> tmp))) {
            cout << "\x1b[31m" << "error: " << "\033[0m" << "invalid values" << endl << endl;
            continue;
        }
        break;
    }
    cout << endl;

    /******** Build the basis ********/

    shared_ptr<AbstractBasis> basis;
    // Points for interpolation
    Matrix xy;
    // Points for approximation
    Matrix XY;
    // Perturbation
    random_device rd;
    mt19937 gen(rd());
    normal_distribution<> shift(0.0, sigma);

    // Interpolation
    if(method == interpol) {
        double step = (x_r - x_l) / (n - 1);
        {
        double arr_xy[2*n];
        for(size_t i = 0; i < n; ++i) {
            double x;
            if(i == 0) {
                x = x_l;
            } else if(i == n - 1) {
                x = x_r;
            } else {
                x = x_l + i * step;
            }
            double y = f(x);
            arr_xy[2*i] = x;
            arr_xy[2*i + 1] = y + shift(gen);
        }
        xy = Matrix(arr_xy, n, 2);
        }

        if(basis_type == polynomial) {
            basis = shared_ptr<AbstractBasis>(new PolynomialBasis(xy, n));
        } else if (basis_type == lagrange) {
            basis = shared_ptr<AbstractBasis>(new LagrangeBasis(xy));
        } else { // if basis_type == hermite
            Matrix xydy(n, 3);
            {
            double arr_dy[n];
            double dx = 1e-3 * step;
            for(size_t i = 0; i < n; ++i) {
                double x = xy(i+1, 1);
                arr_dy[i] = derivative(x, dx, f);
            }
            Matrix dy(arr_dy, n, 1);
            xydy.emplace(xy, 1, 1)
                .emplace(dy, 1, 3);
            }
            basis = shared_ptr<AbstractBasis>(new HermiteBasis(xydy));
        }
    // Approximation
    } else { // if method == approx
        {
        double arr_XY[2*m];

        double step_X = (x_r - x_l) / (m - 1);
        for(size_t i = 0; i < m; ++i) {
            double X;
            if(i == 0) {
                X = x_l;
            } else if(i == m - 1) {
                X = x_r;
            } else {
                X = x_l + i * step_X;
            }
            double Y = f(X) + shift(gen);
            arr_XY[2*i] = X;
            arr_XY[2*i + 1] = Y;
        }
        XY = Matrix(arr_XY, m, 2);
        }

        if(basis_type == polynomial) {
            basis = shared_ptr<AbstractBasis>(new PolynomialBasis(XY, n));
        } else { // if(basis_type == lagrange || basis_type == hermite)
            // calculate reference basis points
            Matrix x(n, 1);
            {
            double step_x =  (x_r - x_l) / (n - 1);
            for(size_t i = 0; i < n; ++i) {
                double x_i;
                if(i == 0) {
                    x_i = x_l;
                } else if(i == m - 1) {
                    x_i = x_r;
                } else {
                    x_i = x_l + i * step_x;
                }
                // matrix indices start from 1
                x(i + 1) = x_i;
            }
            }

            if(basis_type == lagrange)  {
                basis = shared_ptr<AbstractBasis>(new LagrangeBasis(XY, x));
            } else { // if basis_type == hermite
                basis = shared_ptr<AbstractBasis>(new HermiteBasis(XY, x));
            }
        }
    }

    /******** Plot the result ********/

    QApplication app(argc, argv);

    QVector<double> vx;
    QVector<double> vy;
    if(method == interpol) {
        for(size_t i = 1; i <= n; ++i) {
            vx.append(xy(i,1));
            vy.append(xy(i,2));
        }
    } else { // if(method == approx)
        for(size_t i = 1; i <= m; ++i) {
            vx.append(XY(i,1));
            vy.append(XY(i,2));
        }
    }

    QString title;
    title += (basis_type == polynomial) ? "Canonical polynomial "
        : ((basis_type == lagrange) ? "Lagrange " : "Hermite ");
    title += (method == interpol) ? "interpolation" : "approximation";
    ((title += " of a function ") += fun_str.c_str()) += " by ";
    (title += QString().setNum(m)) += " points";

    MainWindow mw(f, qMakePair(vx, vy), sigma, basis);
    mw.setWindowTitle(title);
    mw.show();

    return app.exec();
}
