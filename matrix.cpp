#include <iostream>
#include <cassert>
#include "matrix.h"

using std::ostream;
using std::slice;

namespace polynom {

//Constructeurs
Matrix::Matrix(size_t m, size_t n): _m(m), _n(n), _vad(0.0, m*n)
{}
Matrix::Matrix(double t[], size_t m, size_t n): _m(m), _n(n), _vad(t, m*n)
{}
Matrix::Matrix(const vad& va, size_t m, size_t n): _m(m), _n(n), _vad(va)
{}
Matrix::Matrix(const Matrix& m): _m(m._m), _n(m._n), _vad(m._vad)
{}

Matrix Matrix::operator-() const
{
    return Matrix(-_vad, _m, _n);
}

Matrix& Matrix::operator+=(const Matrix& rhs)
{
    assert (_m == rhs._m);
    assert (_n == rhs._n);
    _vad += rhs._vad;
    return *this;
}

Matrix& Matrix::operator-=(const Matrix& rhs)
{
    assert (_m == rhs._m);
    assert (_n == rhs._n);
    _vad -= rhs._vad;
    return *this;
}

Matrix operator*(const Matrix& m1, const Matrix& m2)
{
    assert (m1._n == m2._m);
    Matrix result(m1._m, m2._n);
    for(size_t i=0; i < m1._m; ++i) {
        for (size_t j=0; j < m2._n; ++j) {
            vad tmp = m1._vad[slice(i * m1._n, m1._n, 1)]
                * m2._vad[slice(j, m2._m, m2._n)];
            result._vad[j + i * m2._n] = tmp.sum();
        }
    }
    return result;
}


Matrix& Matrix::operator*=(const Matrix& rhs)
{
    Matrix tmp(*this);
    *this = tmp * rhs;
    return *this;
}

Matrix Matrix::block(std::size_t i, std::size_t j, std::size_t m, std::size_t n) const
{
    // i,j runs form 0 to _m-1/_n-1
    --i;
    --j;
    assert(i >= 0 && i + m <= _m);
    assert(j >= 0 && j + n <= _n);

    vad block(m*n);
    for(size_t i_block = 0; i_block < m; ++i_block, ++i) {
        block[slice(i_block * n, n, 1)] = _vad[slice(i * _n + j, n, 1)];
    }

    return Matrix(block, m, n);
}

Matrix& Matrix::emplace(const Matrix& M, std::size_t i, std::size_t j)
{
    // i,j runs form 0 to _m-1/_n-1
    --i;
    --j;
    assert(i >= 0 && i + M._m <= _m);
    assert(j >= 0 && j + M._n <= _n);
    // this == &M only if i,j == 0 after assertion
    if(this == &M) {return *this;}

    for(size_t i_M = 0; i_M < M._m; ++i_M, ++i) {
        _vad[slice(i * _n + j, M._n, 1)] = M._vad[slice(i_M * M._n, M._n, 1)];
    }
    return *this;
}

ostream& Matrix::afficher(ostream& flux) const
{
    for(size_t i = 0; i < _m; ++i) {
        for (size_t j = 0; j < _n - 1; ++j) {
            flux << _vad[j + i * _n] << '\t';
        }
        flux << _vad[_n - 1 + i * _n];
        flux<<std::endl;
    }
    return flux;
}

Matrix operator+(const Matrix& m1, const Matrix& m2)
{
    Matrix result(m1);
    result += m2;
    return result;
}

Matrix operator-(const Matrix& m1, const Matrix& m2)
{
    Matrix result(m1);
    result -= m2;
    return result;
}

Matrix operator*(double a, const Matrix& m)
{
    Matrix result(m);
    result._vad *= a;
    return result;
}

Matrix operator*(const Matrix& m, double a)
{
    return a * m;
}

Matrix Matrix::trans() const
{
    Matrix result = Matrix(_n, _m);
    for(size_t i = 0; i < _m; ++i) {
        result._vad[slice(i, result._m ,result._n)]
            = _vad[slice(i * _n, result._n, 1)];
    }
    return result;
}

Matrix cong_grad(const Matrix& A, const Matrix& b)
{
    assert(A.m() == A.n());
    assert(A.m() == b.m());
    assert(b.n() == 1);

    size_t n = A.m();

    #ifndef NDEBUG
    for(size_t i = 1; i <= n; ++i) {
        for(size_t j = 1; j < i; ++j) {
            assert(A(i,j) == A(j,i));
        }
    }
    #endif // NDEBUG

    Matrix x = Matrix(n, 1);
    Matrix r = b - A*x;
    Matrix p = r;

    /// Maximal numver of iterations.
    size_t max_iter_nb = 50000;
    /// Maximal relative error.
    double rel_error = 1e-6;
    double b_norm2 = b.norm2();
    size_t iter = 0;
    while((b - A*x).norm2() > b_norm2 * rel_error
          && (iter < max_iter_nb)) {
        double rr = (r,r);
        double alpha = rr / (A*p,p);
        x += alpha * p;
        r -= alpha * A * p;
        double beta = (r,r) / rr;
        p = r + beta * p;
        ++iter;
    }

    #ifndef NDEBUG
    if(iter >= max_iter_nb) {
        std::cerr << "Stop iteration proccess: not convergent." << std::endl;
    } else {
        std::cerr << "SLE was successfully solved at iteration "<< iter << std::endl;
    }
    #endif // NDEBUG

    return x;
}

Matrix gauss_seidel(const Matrix& A, const Matrix& b, double w/* = 1.5*/)
{
    assert(A.m() == A.n());
    assert(A.m() == b.m());
    assert(b.n() == 1);

    const size_t m = b.m();

    Matrix x = Matrix(m, 1);

    /// Maximal numver of iterations.
    size_t max_iter_nb = 50000;
    /// Maximal relative error.
    double rel_error = 1e-6;
    double b_norm2 = b.norm2();
    size_t iter = 0;
    while((b - A*x).norm2() > b_norm2 * rel_error
          && (iter < max_iter_nb)) {
        for(size_t i = 1; i <= m; ++i) {
            double Sum = 0;
            for(size_t j = 1; j < i; ++j) {
                // new x values
                Sum += A(i,j) * x(j);
            }
            for(size_t j = i + 1; j <= m; ++j) {
                // old x values
                Sum += A(i,j) * x(j);
            }
            x(i) = (1 - w) * x(i) + (b(i) - Sum) * w / A(i,i);
        }
        ++iter;
    }

    #ifndef NDEBUG
    if(iter >= max_iter_nb) {
        std::cerr << "Stop iteration proccess: not convergent." << std::endl;
    } else {
        std::cerr << "SLE was successfully solved at iteration "<< iter << std::endl;
    }
    #endif // NDEBUG

    return x;
}

Matrix gauss(const Matrix& A, const Matrix& b)
{
    assert(A.m() == A.n());
    assert(A.m() == b.m());
    assert(b.n() == 1);

    size_t n = A.m();
    Matrix A_ = A;
    Matrix b_ = b;
    double g_ik;
    for(size_t k = 1; k <= n - 1; ++k) {
        for(size_t i = k + 1; i <= n; ++i) {
            g_ik = A_(i,k)/A_(k,k);
            for(size_t j = 1; j <= n; ++j) {
                if(j <= k)
                    A_(i,j) = 0;
                else
                    A_(i,j) -= g_ik*A_(k,j);
            }
            b_(i) -= g_ik*b_(k);
        }
    }

    Matrix x_(n, 1);
    x_(n) = b_(n)/A_(n,n);
    for(size_t i = n - 1; i >= 1; --i) {
        double sum = 0;
        for(size_t j = i + 1; j <= n ; ++j)
            sum += A_(i,j)*x_(j);
        x_(i) = (b_(i) - sum) / A_(i,i);
    }

    return x_;
}

Matrix inv4(const Matrix& A)
{
    assert(A.m() == 4 && A.n() == 4);

    double det =
        A(1,4) * A(2,3) * A(3,2) * A(4,1) - A(1,3) * A(2,4) * A(3,2) * A(4,1) -
        A(1,4) * A(2,2) * A(3,3) * A(4,1) + A(1,2) * A(2,4) * A(3,3) * A(4,1) +
        A(1,3) * A(2,2) * A(3,4) * A(4,1) - A(1,2) * A(2,3) * A(3,4) * A(4,1) -
        A(1,4) * A(2,3) * A(3,1) * A(4,2) + A(1,3) * A(2,4) * A(3,1) * A(4,2) +
        A(1,4) * A(2,1) * A(3,3) * A(4,2) - A(1,1) * A(2,4) * A(3,3) * A(4,2) -
        A(1,3) * A(2,1) * A(3,4) * A(4,2) + A(1,1) * A(2,3) * A(3,4) * A(4,2) +
        A(1,4) * A(2,2) * A(3,1) * A(4,3) - A(1,2) * A(2,4) * A(3,1) * A(4,3) -
        A(1,4) * A(2,1) * A(3,2) * A(4,3) + A(1,1) * A(2,4) * A(3,2) * A(4,3) +
        A(1,2) * A(2,1) * A(3,4) * A(4,3) - A(1,1) * A(2,2) * A(3,4) * A(4,3) -
        A(1,3) * A(2,2) * A(3,1) * A(4,4) + A(1,2) * A(2,3) * A(3,1) * A(4,4) +
        A(1,3) * A(2,1) * A(3,2) * A(4,4) - A(1,1) * A(2,3) * A(3,2) * A(4,4) -
        A(1,2) * A(2,1) * A(3,3) * A(4,4) + A(1,1) * A(2,2) * A(3,3) * A(4,4);

    assert(det != 0);

    double tr = A.tr();

    Matrix A_2 = A*A;
    double tr_2 = A_2.tr();

    Matrix A_3 = A*A_2;
    double tr_3 = A_3.tr();

    double vE[] = {1,0,0,0,
                   0,1,0,0,
                   0,0,1,0,
                   0,0,0,1};
    Matrix E(vE, 4, 4);

    return 1 / det *
        (1/6.0 * (tr*tr*tr - 3*tr*tr_2 + 2*tr_3) * E
         - 0.5 * A * (tr*tr - tr_2) + A_2 * tr - A_3);

}

Matrix inverse(const Matrix& A) {
    assert(A.m() == A.n());

    size_t n = A.n();
    if(n == 1) {
        assert(A(1,1) != 0);
        return Matrix(vad(1./A(1,1), 1), 1, 1);
    }
    if(n == 2) {
        double det = A(1,1) * A(2,2) - A(1,2) * A(2,1);
        assert(det != 0);
        double arr[] = {A(2,2), -A(1,2), -A(2,1), A(1,1)};
        return 1. / det * Matrix(arr, 2, 2);
    }

    /// size of p matrix
    size_t p = (A.m() + 1) / 2;
    size_t s = A.m() - p;

    Matrix P = A.block(1,   1,   p, p);
    Matrix Q = A.block(1,   p+1, p, s);
    Matrix R = A.block(p+1, 1,   s, p);
    Matrix S = A.block(p+1, p+1, s, s);

    Matrix S_inv = inverse(S);
    Matrix QS = Q * S_inv;
    Matrix SR = S_inv * R;

    Matrix P_ = inverse(P - Q * SR);
    Matrix Q_ = -P_ * QS;
    Matrix R_ = -SR * P_;
    Matrix S_ = S_inv + SR * P_ * QS;

    return Matrix(n, n)
        .emplace(P_, 1,   1)
        .emplace(Q_, 1,   p+1)
        .emplace(R_, p+1, 1)
        .emplace(S_, p+1, p+1);
}

} // namespace polynom
