**warning**: a student's home task

An implementation of several polynomial regression (interpolation and approximation) methods in 1D space:

* linear regression (as a special case of the polynomial regression)
* polynomial regression (canonical form)
* Lagrange polynomial regression
* Hermite polynomial regression

![script1.png](https://bitbucket.org/repo/kGak7e/images/2927608724-script1.png)

**Building and launch**

muParser and cmake(>= 2.8.11) should be installed

```
#!bash
cd /path/to/project
mkdir build
cd ./build
cmake ..
make
./polynom

```