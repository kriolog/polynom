#ifndef HERMITE_BASIS_H
#define HERMITE_BASIS_H

#include "abstract_basis.h"

namespace polynom {

class HermiteBasis : public AbstractBasis
{
public:
    /**
     * Approximation constructor.
     * @param XY a data set (X, Y).
     * @param x reference basis points.
     * Data points @a XY should be in the range of points x and should be more
     * or less homogeneously distributed in it (to avoid the void intervals).
     * Number of points @a XY should not be less than the number of basis
     * function (equals to number of reference basis points @a x * 2.
     */
    HermiteBasis(const Matrix& XY, const Matrix& x);

    /**
     * Interpolation constructor.
     * @param xydy reference basis points, function values and its derivatives:
     * {x, y, y'}
     */
    HermiteBasis(const Matrix& xydy);

private:
    /**
     * Value of the @p i 'th Lagrange basis function in the point @p x.
     * Odd functions are Phi, even - are Psi of a correspondent reference
     * point x_((i+1)/2) (indecies start from 1).
     */
    virtual double f(size_t i, double x) const;

    /**
     *  Calculate the coefficients abcd of Hermite basis functions.
     */
    void calc_H();

    /**
     *  Reference basis points.
     */
    Matrix _x;

    /**
     * Coefficients abcd (in rows) of Hermite basis functions in intervals:
     *
     *     (phi,psi)_(1,2),i-1   (phi,psi)_(1,2),i
     * --|---------------------|-------------------|--
     * x_i-1                  x_i                x_i+1
     *
     *  phi_1 psi_1 phi_2 psi_2
     * |  a     a     a     a |            |   x_i^3       x_i^2     x_i     1 |^-1
     * |  b     b     b     b | = M_i^-1 = | 3*x_i^2     2*x_i       1       0 |
     * |  c     c     c     c |            |   x_(i+1)^3   x_(i+1)^2 x_(i+1) 1 |
     * |  d     d     d     d |            | 3*x_(i+1)^2 2*x_(i+1)   1       0 |
     *
     * _H = |M_1^-1|M_2^-1|...|M_i^-1|...|M_(n-1)^-1|
     */
    Matrix _H;

    #ifndef NDEBUG
    /**
     * Checks input validity. All the reference points must be different.
     * The order of points is not important.
     */
    void check_input() const;
    #endif // NDEBUG
};

} // namespace polynom

#endif // HERMITE_BASIS_H
