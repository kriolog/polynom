#include "hermite_basis.h"

#include <cassert>

namespace polynom {

HermiteBasis::HermiteBasis(const Matrix& XY, const Matrix& x):
    AbstractBasis(x.m() * 2),
    _x(x)
{
    assert(x.m() > 1 && x.n() == 1);
    assert(XY.m() >= _n && XY.n() == 2);
    #ifndef NDEBUG
    // There are no two equal x values
    check_input();
    #endif // NDEBUG

    calc_H();
    _coeffs = coeffs_approx(XY);
}

HermiteBasis::HermiteBasis(const Matrix& xydy):
    AbstractBasis(xydy.m() * 2),
    _x(xydy.column(1))
{
    assert(xydy.m() > 1 && xydy.n() == 3);
    #ifndef NDEBUG
    // There are no two equal x values
    check_input();
    #endif // NDEBUG

    calc_H();
    // Coefficients of interpolation are {y_1, y'_1, y_2, y'_2, ...}
    // by definition.
    _coeffs = Matrix(_n, 1);
    for(size_t i = 0; i < _n; ++i) {
        if (i % 2 == 0) {
            // y
            _coeffs(i + 1) = xydy(i / 2 + 1, 2);
        } else {
            // y'
            _coeffs(i + 1) = xydy(i / 2 + 1, 3);
        }
    }
}

double HermiteBasis::f(size_t i, double x) const
{
    // Now i runs from 0 to _n - 1.
    --i;
    assert(i >= 0 && i < _n);
    assert(x >= _x(1) && x <= _x(_x.m()));

    // Number of reference point, runs from 0 to _x.m() - 1.
    size_t j = i / 2;
    // Shift of the final position of index:
    // p:  -2      -1        0     1
    //   Phi_j-1 Psi_j-1 | Phi_j Psi_j
    //                  x_j
    size_t s = i % 2;

    if(x == _x(j + 1)) {
        return i % 2
            // Psi_j(x_j)=0
            ? 0.0
            // Phi_j(x_j)=1
            : 1.0;
    }
    if(x >= _x(j + 1)) { // _x((j) + 1)
        if(j + 1 >= _x.m() || x >= _x(j + 2)) { // _x((j + 1) + 1)
            return 0;
        }
    } else { // if(x < _x(j + 1))
        if(j - 1 < 0 || x <= _x(j)) { // _x((j - 1) + 1)
            return 0;
        }
        s -= 2;
    }

    // return [x^3, x^2, x, 1] * _H.column((4 * j + s) +1)
    double x_2 = x * x;
    double x_3 = x * x_2;
    double X[] = {x_3, x_2, x, 1};

    return Matrix(X, 4) , _H.column((4 * j + s) + 1);
}

void HermiteBasis::calc_H()
{
    _H = Matrix(4, 4 * (_x.m() - 1));

    // from 1 to _x.m() - 1
    for(size_t i = 1; i < _x.m(); ++i) {
        // x_i, x_i^2, x_i^3
        double x_i = _x(i);
        double x_i_2 = x_i * x_i;
        double x_i_3 = x_i * x_i_2;
        // x_(i+1), x_(i+1)^2, x_(i+1)^3
        double x_ip1 = _x(i+1);
        double x_ip1_2 = x_ip1 * x_ip1;
        double x_ip1_3 = x_ip1 * x_ip1_2;

        double dm[] = {    x_i_3,       x_i_2,   x_i,   1,
                       3 * x_i_2,   2 * x_i,     1,     0,
                           x_ip1_3,     x_ip1_2, x_ip1, 1,
                       3 * x_ip1_2, 2 * x_ip1,   1,     0};
        Matrix M(dm, 4, 4);
        _H.emplace(inverse(M), 1, 1 + 4 * (i - 1));
    }
}

#ifndef NDEBUG
void HermiteBasis::check_input() const
{
    size_t m = _x.m();
    for(size_t i = 1; i <= m; ++i) {
        for(size_t j = i + 1; j <= m; ++j) {
            assert(_x(i) != _x(j));
        }
    }
}
#endif // NDEBUG

} // namespace polynom
