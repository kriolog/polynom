#ifndef MATRIX_H
#define MATRIX_H

#include <valarray>
#include <cassert>
#include <iosfwd>

using std::size_t;

typedef std::valarray<double> vad;

namespace polynom {

class Matrix
{
public :
    /// Zero matrix
    Matrix(size_t m = 1, size_t n = 1);
    Matrix(double t[], size_t m, size_t n = 1);
    Matrix(const vad& va, size_t m, size_t n);
    Matrix(const Matrix& m);


    size_t m() const { return _m; }
    size_t n() const { return _n; }
    Matrix line(size_t i) const {
        assert ((i > 0) && (i <= _m));
        return Matrix(_vad[std::slice((i - 1) * _n, _n, 1)], 1, _n);
    }

    Matrix column(size_t j) const {
        assert ((j > 0) && (j <= _n));
        return Matrix(_vad[std::slice((j - 1), _m ,_n)], _m, 1);
    }
    /// Join all lines and devide than resultant line into m lines for create
    /// new matrix.
    Matrix to_matrice(size_t m, size_t n) const {
        assert (_m * _n == m * n);
        return Matrix(_vad, m , n);
    }
    const double& operator()(size_t i, size_t j = 1) const    {
        assert ((i > 0) && (i <= _m));
        assert ((j > 0) && (j <= _n));
        return _vad[(j-1) + (i-1)*_n];
    }
    double& operator()(size_t i, size_t j = 1) {
        return const_cast<double&>(static_cast<const Matrix&>(*this)(i, j));
    }
    Matrix operator-() const;
    Matrix& operator+=(const Matrix& rhs);
    Matrix& operator-=(const Matrix& rhs);
    Matrix& operator*=(const Matrix& rhs);
    /// inner product for 2 matrices n * 1 or for 2 matrices 1 * n
    double operator,(const Matrix& m) const {
        assert((_m == 1 && m._m == 1) || (_n == 1 && m._n == 1));
        assert((_m == m._m) && (_n == m._n));
        return (_vad * m._vad).sum();
    }

    /// Cut a block which is a matrix of size @a m x @a n with @a i, @a j
    /// beginning (left top) position in this matrix.
    Matrix block(size_t i, size_t j, size_t m, size_t n) const;

    /// Replace a block of this matrix to the matrix @a M; Values of cells of
    /// the block are replaced by the corresponding values of the matrix @a M.
    /// The cell (1,1) of the matrix @a M corresponds to the cell (i,j) of
    /// this matrix.
    Matrix& emplace(const Matrix& M, size_t i, size_t j);

    double sum() const { return _vad.sum(); }
    /// Trace for squared matrices.
    double tr() const {
        assert(_m == _n);
        return (_vad[std::slice(0, _n, _n+1)]).sum();
    }
    Matrix abs() const { return Matrix(std::abs(_vad), _m, _n); }
    double min() const { return _vad.min(); }
    double max() const { return _vad.max(); }
    double norm1() const { return std::abs(_vad).sum(); }
    /// racine de la somme des m(i,j)^2
    double norm2() const { return sqrt((_vad*_vad).sum()); }
    double normInf() const { return (std::abs(_vad)).max(); }
    Matrix trans() const;

    friend Matrix operator* (const Matrix& m1, const Matrix& m2);
    friend Matrix operator* (double a, const Matrix& m);

    friend std::ostream& operator<< (std::ostream& flux, const Matrix& m) {
        return m.afficher(flux);
    }

 private:
    vad _vad; ///valarray de double contenant les valeurs de la matrice
    size_t _m; ///nombre de lignes
    size_t _n; ///nombre de colonnes
    ///surcharge operator << pour matrice et valarray
    std::ostream& afficher(std::ostream& flux) const;
};

Matrix operator+ (const Matrix& m1, const Matrix& m2);
Matrix operator- (const Matrix& m1, const Matrix& m2);
Matrix operator- (const Matrix& m1, const Matrix& m2);
Matrix operator* (const Matrix& m1, const Matrix& m2);
Matrix operator* (double a, const Matrix& m);
Matrix operator* (const Matrix& m, double a);

/// Gauss solution for linear system A*x=b.
Matrix gauss(const Matrix& A, const Matrix& b);

/**
 *  Implementation of a conjugate gradient method.
 *  Approximate solution of the algebraic linear system A*x=b.
 *  A must be symmetric and positive-definite.
 */
Matrix cong_grad(const Matrix& A, const Matrix& b);

/// Gaus-Seidel iterative method (successive over-relaxation veriation).
Matrix gauss_seidel(const Matrix& A, const Matrix& b, double w = 1.5);

/// Inversion of matrix 4x4.
Matrix inv4(const Matrix& A);

/// Inversion by partitioning:
///     |P | Q|
/// A = |--+--|
///     |R | S|
Matrix inverse(const Matrix& A);

} // namespace polynom

#endif // MATRIX_H
