#include "abstract_basis.h"

#include <cassert>

namespace polynom {

AbstractBasis::AbstractBasis(size_t n): _n(n), _coeffs(Matrix())
{
    assert(n > 1);
}

double AbstractBasis::operator()(double x) const
{
    double result = 0;
    for(size_t i = 1; i <= _n; ++i) {
        result += _coeffs(i) * f(i, x);
    }

    return result;
}

Matrix AbstractBasis::coeffs_interpol(const Matrix& XY)
{
    assert(XY.m() == _n);
    assert(XY.n() == 2);

    Matrix A = this->A(XY.column(1));
    Matrix B = XY.column(2);

    return gauss_seidel(A, B);
}

Matrix AbstractBasis::coeffs_approx(const Matrix& XY)
{
    assert(XY.m() >= _n);
    assert(XY.n() == 2);

    Matrix A = this->A(XY.column(1));
    Matrix B = XY.column(2);
    Matrix A_t = A.trans();

    return gauss_seidel(A_t * A, A_t * B);
}

Matrix AbstractBasis::A(const Matrix& x) const
{
    assert(x.m() >= _n);
    assert(x.n() == 1);

    size_t m = x.m();
    Matrix result(m, _n);
    for(size_t i = 1; i <= m; ++i) {
        for(size_t j = 1; j <= _n; ++j) {
            result(i,j) = f(j, x(i));
        }
    }

    return result;
}

} // namespace polynom
