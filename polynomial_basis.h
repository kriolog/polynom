#ifndef POLYNOMIAL_BASIS_H
#define POLYNOMIAL_BASIS_H

#include "abstract_basis.h"

#include "matrix.h"

namespace polynom {

/**
 * Decomposition by a canonical polynomial basis (x^(i-1), i = [1, n]).
 */
class PolynomialBasis: public AbstractBasis
{
public:
    /**
     * Approximation or interpolation constructor, depending on the size of
     * @p XY: if size of XY == m - interpolation, otherwise
     * approximation. Size of XY should not be less than m, both should be
     * positive.
     * @param XY a data set (X, Y).
     * @param n number of basis functions.
     * Number of points @a XY should not be less than the number of basis
     * functions.
     */
    PolynomialBasis(const Matrix& XY, size_t n);

private:
    /**
     * Value of the @p i 'th basis function in the point @p x (x^(i-1)).
     */
    virtual double f(size_t i, double x) const;
};

} // namespace polynom

#endif // POLYNOMIAL_BASIS_H
