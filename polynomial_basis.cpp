#include "polynomial_basis.h"

#include <cassert>
#include <cmath>

namespace polynom {

PolynomialBasis::PolynomialBasis(const Matrix& XY, size_t n):
    AbstractBasis(n)
{
    assert(n > 1);
    assert(XY.m() >= _n && XY.n() == 2);

    _coeffs = (XY.m() == n) ? coeffs_interpol(XY) : coeffs_approx(XY);
}

double PolynomialBasis::f(std::size_t i, double x) const
{
    assert(i > 0 && i <= _n);

    return pow(x, i - 1);
}

} // namespace polynom
