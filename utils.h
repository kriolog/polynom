#ifndef UTILS_H
#define UTILS_H

#include "muParser.h"

namespace polynom {

/// A wrapper around muParser function of one variable
class MuParser1DFunc
{
public:
    MuParser1DFunc(double& x, mu::Parser& f): _x(x), _f(f) {}
    double operator()(double x) const {
        _x = x;
        return _f.Eval();
    };
private:
    mu::Parser& _f;
    /*"mutable"*/ double& _x;
};

/// Derivative of the function @a f in the point @a x.
/// Copied from boost.org
template <class F>
static double derivative(double x, double dx, F f) {
   const double dx1 = dx;
   const double dx2 = dx1 * 2;
   const double dx3 = dx1 * 3;

   const double m1 = (f(x + dx1) - f(x - dx1)) / 2;
   const double m2 = (f(x + dx2) - f(x - dx2)) / 4;
   const double m3 = (f(x + dx3) - f(x - dx3)) / 6;

   const double fifteen_m1 = 15 * m1;
   const double six_m2     =  6 * m2;
   const double ten_dx1    = 10 * dx1;

   return ((fifteen_m1 - six_m2) + m3) / ten_dx1;
}

} // namespace polynom

#endif // UTILS_H
