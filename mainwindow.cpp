#include <cassert>

#include "mainwindow.h"

#include "qcustomplot.h"

#include "utils.h"
#include "abstract_basis.h"

namespace polynom {

MainWindow::MainWindow(
        const MuParser1DFunc& f,
        const QPair<QVector<double>, QVector<double> >& pts_discr,
        double sigma,
        std::shared_ptr<AbstractBasis> basis,
        QWidget* parent /* = 0*/)
    : QMainWindow(parent)
{
    assert(pts_discr.first.size() >= 2);
    assert(sigma >= 0);

    setGeometry(300, 150, 540, 330);
    statusBar()->clearMessage();
    QCustomPlot* plot = new QCustomPlot(this);
    plot->setMinimumSize(240,180);
    setCentralWidget(plot);

    // Prepare the data
    QVector<double> x_discr = pts_discr.first;
    QVector<double> y_discr = pts_discr.second;

    double x_left;
    double x_right;
    {
    QVector<double> x_discr_sorted = x_discr;
    qSort(x_discr_sorted.begin(), x_discr_sorted.end());
    x_left = x_discr_sorted.first();
    x_right = x_discr_sorted.last();
    }

    int segments_nb = 256;
    double step = (x_right - x_left) / segments_nb;
    // x vector of continuous functions (theory, regression)
    QVector<double> x_func(segments_nb + 1);
    // y vector of theory function
    QVector<double> y_theor(segments_nb + 1);
    // y vector of regression function
    QVector<double> y_regr(segments_nb + 1);
    // y vector of a upper bound of the one sigma (68%) confidence interval
    QVector<double> y_upp(segments_nb + 1);
    // y vector of a lower bound of the one sigma (68%) confidence interval
    QVector<double> y_low(segments_nb + 1);
    for(int i = 0; i < segments_nb + 1; ++i)
    {
        x_func[i] = x_left + i * step;
        y_theor[i] = f(x_func[i]);
        y_regr[i] = (*basis)(x_func[i]);
        y_upp[i] = y_theor[i] + sigma;
        y_low[i] = y_theor[i] - sigma;
    }

    // Bandwidth
    QPen bw_pen;
    bw_pen.setStyle(Qt::DotLine);
    bw_pen.setWidth(1.0);
    bw_pen.setColor(QColor(180,180,180));
    // Upper bound
    plot->addGraph();
    plot->graph(0)->setPen(bw_pen);
    plot->graph(0)->setBrush(QBrush(QColor(50,30,255,20)));
    plot->graph(0)->setData(x_func, y_upp);
    // Don't show the confidence band in legend
    plot->legend->removeItem(plot->legend->itemCount()-1);
    // Lower bound
    plot->addGraph();
    plot->graph(1)->setPen(bw_pen);
    plot->graph(1)->setData(x_func, y_low);
    plot->graph(0)->setChannelFillGraph(plot->graph(1));
    // Don't show the confidence band in legend
    plot->legend->removeItem(plot->legend->itemCount()-1);

    // Approximation / interpolation function (regression)
    plot->addGraph();
    plot->graph(2)->setPen(QPen(QBrush(Qt::red), 2.0));
    plot->graph(2)->setData(x_func, y_regr);
    plot->graph(2)->setName("Regression");

    // Theory function
    plot->addGraph();
    plot->graph(3)->setPen(QPen(QBrush(Qt::blue), 1.0));
    plot->graph(3)->setData(x_func, y_theor);
    plot->graph(3)->setName("Theory");

    // Discrete points with uncertainty
    plot->addGraph();
    plot->graph(4)->setData(x_discr, y_discr);
    plot->graph(4)->setPen(QPen(QBrush(Qt::blue), 1.5));
    plot->graph(4)->setLineStyle(QCPGraph::lsNone);
    plot->graph(4)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross));
    plot->graph(4)->setName("Measurement");

    plot->xAxis->setLabel("x");
    plot->yAxis->setLabel("f(x)");

    plot->rescaleAxes(true);
    plot->yAxis->scaleRange(1.05, plot->yAxis->range().center());
    plot->xAxis->scaleRange(1.05, plot->xAxis->range().center());
      // make top right axes clones of bottom left axes. Looks prettier:
    plot->axisRect()->setupFullAxesBox();

    plot->legend->setVisible(true);
    plot->legend->setFont(QFont("Helvetica", 8));
    plot->legend->setRowSpacing(-3);
    plot->legend->setColumnSpacing(-3);
    plot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignBottom|Qt::AlignRight);

    plot->replot();
}

} // namespace polynom
