#ifndef BASIS_H
#define BASIS_H

#include <cstddef>

#include "matrix.h"

namespace polynom {

/**
 * Decomposition of a continious function by a finite basis. The function is
 * constructed by a set of discrete data (interpolation or aproximation in
 * points).
 */
class AbstractBasis
{
public:
    /**
     * @return a value of the decompsed function in the point @a x.
     */
    virtual double operator()(double x) const;

    virtual ~AbstractBasis() {}

protected:
    /**
     * @param m a number of basis functions (should be greater than 1).
     */
    AbstractBasis(size_t n);

    /**
     * Value of the @p i 'th basis function in the point @p x.
     * @param i an index of the Lagrange function in basis.
     * @param x a point for which the function value will be calculated.
     * @return a value of the @a i 'th basis function in the point @a x.
     */
    virtual double f(size_t i, double x) const = 0;

    /**
     * Coefficients of basis from the interpolation by @p XY point set.
     * Number of points @a XY should not be less than the number of basis
     * functions _n.
     */
    Matrix coeffs_interpol(const Matrix& XY);

    /**
     * Coefficients of basis from the approximation by @p XY point set.
     * Number of points @a XY should be equal to the number of basis
     * functions _n.
     */
    Matrix coeffs_approx(const Matrix& XY);

    /**
     * A number of basis functions.
     */
    size_t _n;

    /**
     * Coefficients of decomposition by basis functions; should be defined in
     * the derived class constructiors.
     */
    Matrix _coeffs;

private:
    // Matrix { f_j(x_i) }, where f's are the basis functions.
    Matrix A(const Matrix& x) const;
};

} // namespace polynom

#endif // BASIS_H
